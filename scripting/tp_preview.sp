#pragma semicolon 1

#include <sdktools>

#define PLUGIN_VERSION	"1.1.85"

Handle hTimer[MAXPLAYERS+1];		// Если != INVALID_HANDLE значит таймер работает, значит игрок смотрит на себя
bool g_bTP[MAXPLAYERS+1];
bool g_bFullTP[MAXPLAYERS+1];
bool g_bOwnFog;
int g_iFog = -1;
int g_iFog2 = -1;

public Plugin myinfo =
{
    name        = "[Shop] TPerson & PreView",
    author      = "ShaRen",
    description = "Equipments component for shop",
    version     = PLUGIN_VERSION,
    url         = "Servers-Info.Ru"
}

public OnPluginStart()
{
	//HookEvent("player_spawn", Event_PlayerSpawn);
	HookEvent("round_start", Event_RoundStart);
}

public Event_RoundStart(Event e, const String:name[], bool:dontBroadcast)
{
	for (int i=1; i<MAXPLAYERS; i++) {
		if (hTimer[i] != INVALID_HANDLE) {
			KillTimer(hTimer[i]);
			hTimer[i] = INVALID_HANDLE;
		}
		if (IsValidEntity(i) && IsClientInGame(i)) {
			ClientCommand(i, "firstperson;cam_idealyaw 0; cam_idealdist 150;");
			SetVariantString("Shop_Fog2");
			AcceptEntityInput(i, "SetFogController");
		}
		g_bTP[i] = false;
		g_bFullTP[i] = false;
	}
}

public OnMapEnd()
{
	for (int i=1; i<=MAXPLAYERS; i++)
		hTimer[i] = INVALID_HANDLE;
}

public OnClientConnected(client)
{
	if (IsValidEntity(client) && IsClientInGame(client)) {
		ClientCommand(client, "firstperson;cam_idealyaw 0; cam_idealdist 150;");
		g_bTP[client] = false;
		g_bFullTP[client] = false;
	}
}

public OnClientDisconnect_Post(client)
{
	if (hTimer[client] != INVALID_HANDLE) {
		KillTimer(hTimer[client]);
		hTimer[client] = INVALID_HANDLE;
	}
}

public Action SetBackMode(Handle timer, any client)
{
	hTimer[client] = INVALID_HANDLE;
	if (IsValidEntity(client) && IsClientInGame(client)) {
		ClientCommand(client, "firstperson;cam_idealyaw 0; cam_idealdist 150;");
		SetVariantString("Shop_Fog2");
		AcceptEntityInput(client, "SetFogController");
	}
	g_bTP[client] = false;
	g_bFullTP[client] = false;
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("IsTP", Native_IsTP);
	CreateNative("SetTP", Native_SetTP);
	RegPluginLibrary("warden");
	return APLRes_Success;
}

public int Native_IsTP(Handle plugin, int numParams)
{
	return g_bTP[GetNativeCell(1)];
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon)
{
	if(g_bTP[client] && hTimer[client] != INVALID_HANDLE && (buttons & IN_FORWARD || buttons & IN_LEFT || buttons & IN_RIGHT || buttons & IN_BACK)) {
		KillTimer(hTimer[client]);
		hTimer[client] = INVALID_HANDLE;
		if (IsValidEntity(client) && IsClientInGame(client)) {
			ClientCommand(client, "firstperson; cam_idealyaw 0; cam_idealdist 150;");
			SetVariantString("Shop_Fog2");
			AcceptEntityInput(client, "SetFogController");
			g_bTP[client] = false;
			g_bFullTP[client] = false;
		}
	}
}

/*********************************************************
 * returns if client is in ThirdPerson
 *
 * @param client		The client to switch TP
 * @param bTP			Set TP on or off
 * @param bPreView		Set TP in preView mod (with fog)
 * @param fTime			if bPreView set reset timer
 * @true on match, false if not		
 *********************************************************/
//native bool SetTP(int client, bool bTP, bool bPreView=false, float fTime=0.0);

public int Native_SetTP(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	if (IsValidEntity(client) && IsClientInGame(client)) {
		if (GetNativeCell(2)) {					// bTP если включает вид от 3 лица
			if (IsPlayerAlive(client))
				if (GetNativeCell(3)) {			// bPreView если это превью
					if (!g_bFullTP[client]) {	// и игрок не играл от 3 лица
						ClientCommand(client, "thirdperson; cam_idealyaw 195; cam_idealdist 70;");
						g_bTP[client] = true;
						SetVariantString("Shop_Fog");
						AcceptEntityInput(client, "SetFogController");
						float fTime = GetNativeCell(4);
						if (fTime) {			// reload timer
							if (hTimer[client] != INVALID_HANDLE) {
								KillTimer(hTimer[client]);
								hTimer[client] = INVALID_HANDLE;
							}
							hTimer[client] = CreateTimer(fTime, SetBackMode, client, TIMER_FLAG_NO_MAPCHANGE);
						}
					}							// если был от 3 лица, то пусть дальше играет от 3 лица
				} else {
					ClientCommand(client, "thirdperson; cam_idealyaw 0; cam_idealdist 130;");
					g_bFullTP[client] = true;
					g_bTP[client] = true;
					if (hTimer[client] != INVALID_HANDLE) {
						KillTimer(hTimer[client]);
						hTimer[client] = INVALID_HANDLE;
					}
				}
		} else {
			ClientCommand(client, "firstperson; cam_idealyaw 0; cam_idealdist 150;");
			g_bFullTP[client] = false;
			g_bTP[client] = false;
			SetVariantString("Shop_Fog2");
			AcceptEntityInput(client, "SetFogController");
		}
		return true;
	}
	return false;
}

public OnPluginEnd()
{
	if (IsValidEntity(g_iFog))
		AcceptEntityInput(g_iFog, "kill");

	if (g_bOwnFog && IsValidEntity(g_iFog2))
		AcceptEntityInput(g_iFog2, "kill");
}

public OnMapStart()
{
	g_iFog2 = FindEntityByClassname(-1, "env_fog_controller");
	if (g_iFog2 != -1 ) {		// если есть на карте туман, то сохранаяем его
		char sTargetName[64];
		GetEntPropString(g_iFog2, Prop_Data, "m_iName", sTargetName, sizeof(sTargetName));

		if (sTargetName[0] == '\0') {
			strcopy(sTargetName, sizeof(sTargetName), "Shop_Fog2");
			DispatchKeyValue(g_iFog2, "targetname", sTargetName);
		}
	} else {		// если нету то создает свой
		g_bOwnFog = true;
		g_iFog2 = CreateEntityByName("env_fog_controller");

		if (g_iFog2 != -1) {
			DispatchKeyValue	 (g_iFog2, "targetname", "Shop_Fog2");
			DispatchKeyValueFloat(g_iFog2, "fogmaxdensity", 0.0);
			DispatchSpawn(g_iFog2);

			AcceptEntityInput(g_iFog2, "TurnOn");
		}
	}

	g_iFog = CreateEntityByName("env_fog_controller");

	if (g_iFog != -1) {
		DispatchKeyValue	 (g_iFog, "targetname", "Shop_Fog");
		DispatchKeyValue	 (g_iFog, "fogenable", "1");
		DispatchKeyValue	 (g_iFog, "fogcolor",  "80 80 80");
		DispatchKeyValueFloat(g_iFog, "fogstart", 100.0);
		DispatchKeyValueFloat(g_iFog, "fogend", 128.0);
		DispatchKeyValueFloat(g_iFog, "fogmaxdensity", 1.0);
		DispatchSpawn(g_iFog);

		AcceptEntityInput(g_iFog, "TurnOn");
	}
}