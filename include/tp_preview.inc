#if defined _tp_preview_included
  #endinput
#endif
#define _tp_preview_included


/*********************************************************
 * returns if client is in ThirdPerson
 *
 * @param client		The client to run the check on
 * @true on match, false if not		
 *********************************************************/
native bool IsTP(int client);

/*********************************************************
 * returns if client is in ThirdPerson
 *
 * @param client		The client to switch TP
 * @param bTP			Set TP on or off
 * @param bPreView		Set TP in preView mod (with fog)
 * @param fTime			if bPreView set reset timer
 * @true on match, false if not		
 *********************************************************/
native bool SetTP(int client, bool bTP, bool bPreView=false, float fTime=0.0);